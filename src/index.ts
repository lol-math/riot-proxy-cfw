export interface Env {
  RIOT_API_KEY: string;
}

export default {
  async fetch(
    request: Request,
    env: Env,
    ctx: ExecutionContext
  ): Promise<Response> {
    const url = new URL(request.url);

    const riotApiUrlString = url.searchParams.get("apiurl") ?? "";
    if (!riotApiUrlString) {
      return new Response("Please provide a valid apiurl");
    }
    const riotApiUrl = new URL(riotApiUrlString);

    if (!riotApiUrl.hostname.endsWith(".riotgames.com")) {
      return new Response(
        "Please provide a riotgames domain to the apiurl parameter"
      );
    }

    request = new Request(riotApiUrl.href, request);
    request.headers.set("Origin", riotApiUrl.origin);
    request.headers.set("X-Riot-Token", env.RIOT_API_KEY);
    let response = await fetch(request);

    // Recreate the response so we can modify the headers
    response = new Response(response.body, response);

    // Set CORS headers
    response.headers.set("Access-Control-Allow-Origin", "*");

    // Append to/Add Vary header so browser will cache response correctly
    response.headers.append("Vary", "Origin");

    return response;
  },
};
